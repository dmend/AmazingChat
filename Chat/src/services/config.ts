import { Injectable } from '@angular/core';

@Injectable()
export class ConfigService {

	// ------------------------BASE CLIENT URLS------------------------------

	// ---BASE---
    public static base = 'http://localhost:3001/';  
    public static login = 'api/login';
    public static signup = 'api/signup';  
    public static getTopics = 'api/topics';
    public static postTopic = 'api/topic';
    public static getUser =function(userId) {return `api/user/${userId}`}
    public static getMessages = function(topic,counter){return `api/messages/${topic}/${counter}`;}
    public static getTotalMessages = function (topic){return `api/totalmessages/${topic}`}

}
