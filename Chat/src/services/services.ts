import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response, HttpModule, ResponseContentType, ResponseType  } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/Rx' ;
import { DomSanitizer } from '@angular/platform-browser';
import { Subject } from 'rxjs/Subject'
import { Observable } from 'rxjs/Observable';
import {ConfigService} from './config';
import { Storage } from '@ionic/storage';


@Injectable()
export class SharedService {

	//VAIRABLES

	private baseWepApiUrl: string;
	private baseAssetUrl: string;
	private currentUser: any;
	private token: string;
	public configObservable = new Subject<boolean>();

	//CONSTRUCTORS

	constructor(
        public http: Http,
        public storage:Storage) { 
		this.baseWepApiUrl = ConfigService.base;
	}

	//METHODS

	//Security - get user's permission for a specific section
	public GetSectionPermission(section) {
		var user = JSON.parse(sessionStorage.getItem('currentUser'));
		var userPermission = user.sectionPermission;
		var sectionPermission: Number = 0;

		userPermission.forEach(element => {
			if (element.Section == section) {
				sectionPermission = element.Permission;
			}	
		});

		return sectionPermission;
	}

    //GET

    public login(url,data) {
		let headers = new Headers();
        headers.append('Content-Type', 'application/json');        
        return this.http.post(this.baseWepApiUrl + url, JSON.stringify(data), {headers: headers}).map((res: Response) => res.json());
	}
	public get(token,url) {

        let headers = new Headers();
        headers.append('Authorization', 'bearer ' + token);
        // headers.append('Content-Type', 'application/json');    

        let options = new RequestOptions({ headers: headers });   
        options.responseType = ResponseContentType.Json;   
        options.withCredentials = true;
         
        return this.http.get(this.baseWepApiUrl + url,options)
            .map(result => result.json());
	}

	//POST
	public post (token, url, data, option?, ) {
        let headers = new Headers();
        headers.append('Authorization', 'bearer ' + token);
        headers.append('Content-Type', 'application/json');        
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.baseWepApiUrl + url, data, options).map((res: Response) => res.json());
        //return this.http.post(this.baseWepApiUrl + url, JSON.stringify(data), {headers: headers}).map((res: Response) => res.json());
        
	}

	//PUT
	public put (url, data, option) {
		// this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
		// this.currentUser == null? this.token = "": this.token = this.currentUser.token;

		// let headers = new Headers();
		// headers.append('Authorization', 'bearer ' + this.token);
		// let options = new RequestOptions({ headers: headers });

        // return this.http.put(this.baseWepApiUrl + url, data, options).map((res: Response) => res.json());
		return this.http.put(this.baseWepApiUrl + url, data).map((res: Response) => res.json());
        
	}

	//DELETE
	public delete (url,option) {
		// this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
		// this.currentUser == null? this.token = "": this.token = this.currentUser.token;

		// let headers = new Headers();
		// headers.append('Authorization', 'bearer ' + this.token);
		//let options = new RequestOptions({ headers: headers });
		//return this.http.delete(this.baseWepApiUrl + url, options).map((res: Response) => res.json());

		return this.http.delete(this.baseWepApiUrl + url).map((res: Response) => res.json());
	}


	



}
 
