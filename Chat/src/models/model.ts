    //Modals for the db
    export class Users  {
        UserID : String;
        _id:String;
        token:String;
        Password:String;
        Role:String
    };
    export class Topics {
        Name: String;
        Description: String;
        fkUserID: String;
        DateCreated: Date;
        _id:String
    };
    export class Messages {
        Text:String;
        fkUserID:String;
        fkTopicID:String;
        DateCreated: Date;
        _id:String;
    };