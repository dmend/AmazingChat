import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {SharedService} from '../services/services';
import {ConfigService} from '../services/config';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {ChatPage} from '../pages/chat/chat';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import {Messages,Topics,Users} from '../models/model';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import {LoginPage} from '../pages/login/login';
const config: SocketIoConfig = { url: 'http://localhost:3001', options: {} };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ChatPage,
    LoginPage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SocketIoModule.forRoot(config),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ChatPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ConfigService,
    SharedService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
