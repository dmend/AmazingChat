import { Component } from '@angular/core';
import { NavController, AlertController,ActionSheetController  } from 'ionic-angular';
import {Observable} from 'rxjs/Observable';
import {ChatPage} from '../chat/chat';
import {Socket} from 'ng-socket-io';
import {SharedService} from '../../services/services';
import {ConfigService} from '../../services/config';
import {Users,Topics} from '../../models/model';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public appTitle = "Amazing Chat!"
  public topics:[Topics];
  public loader;
  public alert;
  public user:Users = null;
  public chatSub;
  constructor(public navCtrl: NavController, 
              public alertCtrl:AlertController, 
              public socket:Socket,
              public sharedService: SharedService,
              private storage: Storage,
              public loadingCtrl: LoadingController,
              public actionSheetCtrl: ActionSheetController
              ) 
  {
    
    this.presentLoading();
    storage.get('user').then((val) => {
      if(val !=null)
      {
        this.user = val
        this.loader.dismiss();
        this.getChats();
      }
      else
      {
        this.navCtrl.setRoot(LoginPage);
      }
    });
    
 
  }
  public presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    this.loader.present();
  }

  public getChats()
  {
    this.sharedService.get(this.user.token,ConfigService.getTopics).subscribe(result=> {
      this.topics = result;
    });
  }
  public getChat ()
  {
    let observable = new Observable (observer => {
      this.socket.on('chat-added', (data)=> {
        observer.next(data);
      });
    });
    return observable;
  }
  public saveChat(data)
  {
    let chat = {DateCreated: new Date(),fkUserID:this.user._id, Name:data.chatName,Description:data.chatDescription};
    this.sharedService.post(this.user.token,ConfigService.postTopic,chat).subscribe(result=> {
        this.socket.emit('add-chat',result)
    });
  }
  public createChatRoom ()
  {

      let prompt = this.alertCtrl.create({
          title:'Create Chat Room',
          inputs: [{
            name:'chatName',
            placeholder:'Chat Name'
          },
        {
          name:'chatDescription',
          placeholder:'Chat Description'
        }],
        buttons:[{
          text:'Cancel',
        },
      {
        text:'Save',
        handler: data => {
          this.saveChat(data);
        }
      }]
        });
        prompt.present();

  }

  public enterChat (topic)
  {

      //this.socket.connect();
      this.socket.disconnect();
      this.chatSub.unsubscribe();
      this.navCtrl.push(ChatPage, {topic:topic, user:this.user});
    
  }
  
  public actions()
  {
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Manage Account',
        buttons: [
          {
            text: 'Log out',
            role: 'destructive',
            handler: () => {
              this.storage.remove('user');
              this.navCtrl.push(LoginPage);
            }
          }
        ]
      });
      actionSheet.present();
  }
  ionViewDidEnter()
  {
    this.socket.connect();
    this.chatSub = this.getChat().subscribe(data=> {
      let topic:Topics = {
        _id: data['_id'],
        DateCreated:data['DateCreated'],
        Description:data['Description'],
        fkUserID:data['fkUserID'],
        Name:data['Name']
      };
      this.topics.push(topic);
    });
  }




}
