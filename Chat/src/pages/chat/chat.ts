import { Component, ViewChild } from '@angular/core';
import {NavParams, ViewController,ToastController,Refresher, Content} from 'ionic-angular';
import {Socket} from 'ng-socket-io';
import {Observable} from 'rxjs/Observable';
import {Messages,Users,Topics} from '../../models/model';
import {SharedService} from '../../services/services';
import {ConfigService} from '../../services/config';
/**
 * Generated class for the ChatComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chat',
  templateUrl: 'chat.html'
})
export class ChatPage {
  @ViewChild(Content) content: Content;
  public user:Users = null;
  public topic:Topics;
  public messages = [];
  public message = '';
  public userSub;
  public messageSub;
  public displayMessagesNumber:number = 10;
  public totalMessagesNumber:number = 0;
  constructor(public viewCtrl: ViewController,
              public params: NavParams, 
              public socket:Socket, 
              public toast:ToastController,
              public sharedServices: SharedService) {
     this.topic = params.get('topic');
     this.user = params.get('user');
     this.joinChat();
     this.getTotalMessages();
     this.getAllMessages();
     this.messageSub = this.getMessages().subscribe(data => {   
            this.messages.push(data);
            if (data['fkUserID'][0]['_id']===this.user._id)
            {
              setTimeout(() => {
                  this.content.scrollToBottom();
              });
            }

     });
     this.userSub = this.getUser().subscribe(data=> {
        let user = data['user'];
        let message = '';
        if (data['event']==='left')
        {
            message = `${user} letf the chat`;
        }
        else
        {
          message = `${user} joined chat`;
        }
        this.showToast(message);
     });
     
  }


 
  public joinChat()
  {
    this.socket.connect();
    this.socket.emit('set-nickname',{nickname:this.user.UserID,topic:this.topic._id});    
  }
  public getTotalMessages()
  {
    this.sharedServices.get(this.user.token,ConfigService.getTotalMessages(this.topic._id)).subscribe(count=> {
      this.totalMessagesNumber = count;
    });
  }
  public getAllMessages(refresher?)
  {
    this.sharedServices.get(this.user.token,ConfigService.getMessages(this.topic._id,this.displayMessagesNumber)).subscribe(response=> {
        this.messages = response.reverse();  
        if (refresher != null)
        {
          refresher.complete();
        }  
        else
        {
            setTimeout(() => {
              this.content.scrollToBottom();
          });
        }
    }); 
  }
  public sendMessage ()
  {
    let data: Messages = {
      DateCreated: new Date(),
      fkTopicID: this.topic._id,
      fkUserID: this.user._id,
      Text: this.message,
      _id:'',
    };
    this.socket.emit('add-message',data);
    this.message = '';
  }

  public getMessages ()
  {
    let observable = new Observable (observer => {
      this.socket.on('message', (data)=> {
        observer.next(data);
      });
    });
    return observable;
  }
  public getUser()
  {
    let observable = new Observable (observer => {
      this.socket.on('users-changed', (data)=> {
        observer.next(data);
      });
    });
    return observable;
  }
  public showToast(text){
    let toast = this.toast.create({
      message:text,
      duration:2000
    });
    toast.present();
  }
  public loadMoreMessages (refresher?)
  {
    if (this.displayMessagesNumber  < this.totalMessagesNumber)
    {
      this.displayMessagesNumber = this.displayMessagesNumber + 10;
      this.getAllMessages(refresher);
    }
  }
  doRefresh(refresher: Refresher) {   
      this.loadMoreMessages(refresher);
     
  }
   public dismiss(){
    this.viewCtrl.dismiss();
  }

  ionViewWillLeave() {
    this.userSub.unsubscribe();
    this.messageSub.unsubscribe();
    this.socket.disconnect();

  }
}
