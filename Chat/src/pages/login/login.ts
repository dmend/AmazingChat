import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {Users} from '../../models/model';
import {SharedService} from '../../services/services';
import {ConfigService} from '../../services/config';
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
   public logInMessage = "Log in";
   public logInInfoMessage = "Not an user? ";
   public signUpMessage = "Sign Up";
   public signUpInfoMessage ="Already an user? ";
   public label = this.logInMessage;
   public user:Users = {UserID: '',
                        Password:'',
                        token:'',
                        _id:'',
                        Role:'user'};
   public infoMessage = this.logInInfoMessage;
   public oppositeLabel = this.signUpMessage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage:Storage,public sharedService:SharedService, public alertCtrl:AlertController) {
     
  }

  public switch() {
    this.label = this.label === this.logInMessage? this.signUpMessage: this.logInMessage;
    this.infoMessage = this.label === this.logInMessage? this.logInInfoMessage : this.signUpInfoMessage;
    this.oppositeLabel = this.label === this.logInMessage? this.signUpMessage: this.logInMessage;
  }
  public action(){
    
    var url = this.label === this.logInMessage ? ConfigService.login : ConfigService.signup;
    this.sharedService.login(url,this.user).subscribe(result=> {
      if(result['success']===true)
      {
        this.storage.remove('user');
        this.user._id = result['_id'];
        this.user.token = result['token'];
        this.storage.set('user',this.user);
        this.navCtrl.setRoot(HomePage);
      }
      else{
        let alert = this.alertCtrl.create({
          title: result['message'],
          buttons: ['Ok']
        });
        alert.present();
      }
    });
  }


}
