let app = require('express')();
var bodyParser = require('body-parser'); 
let http = require('http').Server(app);
let io = require('socket.io')(http);
var mongoose = require('mongoose');
var jwt    = require('jsonwebtoken');
var cors = require('cors');
mongoose.connect('mongodb://localhost/testDB9'); 
var db = mongoose.connection;
//Configuration
var corsOptions = {
    origin: 'http://localhost:8100',
    credentials:true
}
app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
app.use(bodyParser.json());                                     // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:8100");
    res.header('Access-Control-Allow-Methods', 'GET,DELETE, PUT');
    res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials" , "true");
    next();
 });
app.set('secret','ThisAPPLICATIONisTHEBestInTHeWorld');
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    app.post('/api/signup',function (req,res){
       
        Users.find({UserID:req.body.UserID},function(err,user){
            if (err)
            {
                throw err;
            }
           
            if(user.length===0)
            {
               
                var newUser = new Users({
                    UserID:req.body.UserID,
                    Password:req.body.Password,
                    role:'user'
                });
                newUser.save();
                var payload = {
                    role: 'user'
                };
                var token = jwt.sign(payload,app.get('secret'),{
                    expiresIn:"7 days"
                });
                // Users.find({UserID:req.body.UserID},function(err,result){
                    res.json({
                        success:true,
                        message:'Welcome',
                        token:token,
                        _id: newUser._id
                    });
                // });
                
            }
            else
            {
                res.json({success:false,message:"Username already taken"});
            }
        });
    });
    app.post('/api/login',function (req,res){
        Users.findOne({UserID:req.body.UserID, Password: req.body.Password},function (err, user){
            if (err)
            {
                throw err;
            }

            if (!user)
            {
                res.json({success:false,message: "Username or Password don't match."});
            }
            else
            {
                const payload = {
                    role: user.Role
                };
                var token = jwt.sign(payload, app.get('secret'), {
                    expiresIn: "7 days"
                });
                res.json({
                    success:true,
                    message:"Welcome!",
                    token: token,
                    _id:user._id
                });
            }
        });
    });
    app.use(function (req,res,next){

       
        var token = req.body.token || req.params.token || req.headers['x-access-token'] || req.get('Authorization').substring(7) ;
  
        if(token){
            jwt.verify(token,app.get('secret'),function(err,decoded){
                if(err)
                {
                    return res.json({success:false,message:'Failed to authenticate token'});
                }
                else
                {
                    req.decoded = decoded;
                    next();
                }
            });
        }
        else
        {
            return res.status(403).send({
                success:false,
                message:'No token provided'
            });
        }
    });
    //Authenticate Routes
    app.get('/api/topics',function(req,res){

        Topics.find(function (err,topics)
        {
            if (err)
                res.send(err);
            res.json (topics);
        });
    });
    app.get('/api/messages/:topic/:counter', function (req,res){
                let allMessages = Messages.find({fkTopicID:req.params.topic}).populate('fkUserID').sort({DateCreated:'desc'}).limit(Number(req.params.counter)).exec(function(err,response){
                res.json(response);                   
            });
        
    });
    //Return the total message number
    app.get('/api/totalmessages/:topic', function (req,res){
                let allMessages = Messages.find({fkTopicID:req.params.topic}).count().exec(function(err,count){
                res.json(count);                   
            });
        
    });
    app.post('/api/topic',function (req,res){
        var topic = new Topics ({
            Name: req.body.Name,
            Description: req.body.Description,
            fkUserID: req.body.fkUserID,
            DateCreated: req.body.DateCreated
        });
        topic.save();
        res.json(topic);        

    });

    //Socket
    io.on('connection', (socket) => {
        
        socket.on('disconnect', function(){
            io.sockets.in(socket.topic).emit('users-changed', {user: socket.nickname, event: 'left', topic: socket.topic});   
        });
        socket.on('set-nickname', (data) => {
            socket.nickname = data.nickname;
            socket.topic = data.topic;
            socket.join(data.topic);
            io.sockets.in(data.topic).emit('users-changed', {user: data.nickname, event: 'joined'});    
        });
        socket.on('add-message', (message) => {
            var data = new Messages (
                {Text:message.Text,
                 fkUserID: message.fkUserID,
                 fkTopicID: message.fkTopicID,
                 DateCreated: message.DateCreated
            });
            data.save();
            Users.find({_id:message.fkUserID},function(err,result){
                data.fkUserID = result;
                io.sockets.in(message.fkTopicID).emit('message', data);                   
            });     
        });
        socket.on('add-chat',(data)=>{
            io.emit('chat-added',data)
        });
    });
    

});

    //Models
    var Users = mongoose.model('User',{
        UserID : String,
        Password:String,
        Role:String
    });
    var Topics = mongoose.model('Topics',{
        Name: String,
        Description: String,
        fkUserID: String,
        DateCreated: Date
    });
    var Messages = mongoose.model('Messages',{
        Text:String,
        fkUserID: [{type: mongoose.Schema.Types.ObjectId, ref:'User'}],
        fkTopicID:String,
        DateCreated: Date,
    });




var port = process.env.PORT || 3001;
 
http.listen(port, function(){
});