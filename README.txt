Installation:
In order to run the project. First install:
* Node.js
* MongoDB
* Ionic

1. Then use your favorite editor to run the server first that is located inside the BackEnd folder. 
       Command: Node serve.js
Important, make sure that the port is 3001
2. Then after the backend is running, run the front end Ionic app inside the Chat folder. 
       Command: Ionic serve
	If the console displays the following message:
? Looks like a fresh checkout! No ./node_modules directory found. Would you like to install project dependencies?
Please enter Y. Note: This may take a few minutes, please wait.

       Important, make sure that the Ionic app is running on port 8100

